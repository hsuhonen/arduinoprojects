void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.setTimeout(10000);
}


void loop() {
  // put your main code here, to run repeatedly:

  char c = Serial.read();

  if (c >= 0) {
    if (c == 1) {
      Serial.println("case1");  
    }
    else if (c == 2) {
      Serial.println("case2");
    }
    else {
      Serial.println("unknown");
    }
  }
}
