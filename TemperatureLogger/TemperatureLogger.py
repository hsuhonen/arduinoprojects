import serial
import argparse
from datetime import datetime
from time import sleep
from threading import Thread
import matplotlib.pyplot as plt
import numpy as np
from math import ceil
from threading import Lock
import os


# Access lock to guard acces to the data buffers
access_lock = Lock()

buffer_length = 7*24*3600 # one week in seconds
dt_min = 1.0 # minimum available time step is 1 second
N_buffer = int(2 * ceil(buffer_length / dt_min))
display_window = 7200 # seconds for display length

# Define global arrays
i_max = int(round(N_buffer / 2))
time_array = []
channels_array = []

refdate = datetime(2019,1,1)
nb_channels = 1

def main():
    global access_lock
    global N_buffer
    global display_window
    global i_max
    global time_array
    global refdate
    global nb_channels
    global channels_array

    # Read options from the command line
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", type=int, dest="delay", default=1)
    parser.add_argument("-o", type=str, dest="output_file", default="T_logger.output.txt")
    parser.add_argument('-p', action='store_true', dest="display_output")
    parser.add_argument('-n', action='store_true', dest="suppress_graphics")
    parser.add_argument("-c", type=int, dest="nb_channels", default=1)

    args = parser.parse_args()
    outfile_path = args.output_file
    delay = args.delay
    display_output = args.display_output
    suppress_graphics = args.suppress_graphics
    nb_channels = args.nb_channels

    # Initialize the time-array
    time_array = np.linspace(-delay*N_buffer/2,delay*N_buffer/2,N_buffer)
    time_array = time_array - time_array[i_max]
    time_array = time_array + (datetime.now()-refdate).total_seconds()

    # Initialize the channels array
    channels_array = np.zeros([nb_channels,N_buffer])

    read_data(outfile_path)

    # Start the plotter thread unless display not wanted
    if not suppress_graphics:
        gui_thread = Thread(target = plotter_function)
        gui_thread.start()


    with serial.Serial('/dev/ttyACM0',115200,timeout=1) as ser:
        T = [0]*nb_channels
        with open(outfile_path,"a") as outfile:
            while True:            
                timenow_object = datetime.now()
                timenow = timenow_object.strftime("%d%m%Y %H:%M:%S")
                outstring = timenow

                for ch in range(nb_channels):
                    ser.write(chr(ch+1))
                    T[ch] = str(ser.readline()).strip()
                    outstring = outstring + " ; " + T[ch]


                # Begin guarded section
                access_lock.acquire()

                try:
                    if i_max == (N_buffer-1):
                        compact_data()

                    for ch in range(nb_channels):
                        channels_array[ch,i_max+1] = float(T[ch])

                    time_array[i_max+1] = (timenow_object-refdate).total_seconds()
                    i_max = i_max + 1
                except ValueError as _:
                    pass
            
                access_lock.release()
                # End guarded section

                if display_output:
                    print(outstring)

                outfile.write(outstring+"\n")


                sleep(delay)
                
            


def plotter_function():
    global access_lock
    global N_buffer
    global display_window
    global i_max
    global time_array
    global nb_channels
    global channels_array

    # Open a window
    fig = plt.figure()

    ax = [0]*nb_channels
    for ch in range(nb_channels):
        ax[ch] = fig.add_subplot(nb_channels * 100 + 10 + (ch+1))
        ax[ch].autoscale(True)
        ax[ch].title.set_text("Ch " + str(ch+1))
        ax[ch].set_xlabel("Time [s]")
        ax[ch].set_ylabel("T [C]")

    fig.tight_layout()
    fig.show()

    # Periodically update the plt plot
    while True:
        access_lock.acquire()
        i_start = find_start(display_window)

        for ch in range(len(ax)):
            ax[ch].clear()
            ax[ch].plot(time_array[i_start:i_max+1]-time_array[i_max],channels_array[ch,i_start:i_max+1],'r-')
            ax[ch].title.set_text("Ch " + str(ch+1))
            ax[ch].set_xlabel("Time [s]")
            ax[ch].set_ylabel("T [C]")
            ax[ch].margins(0.05)


        
        access_lock.release()

        plt.pause(1)
        


# Shifts the latter half of the data arrays
# to the first half, zeros the latter half,
# and sets i_max = N/2
def compact_data():
    global access_lock
    global N_buffer
    global display_window
    global i_max
    global time_array
    global channels_array
    global nb_channels

    print("Compacting")

    time_array[0:N_buffer/2] = time_array[N_buffer/2:]
    time_array[N_buffer/2:] = 0

    for ch in range(nb_channels):
        channels_array[ch,0:N_buffer/2] = channels_array[ch,N_buffer/2:]
        channels_array[ch,N_buffer/2:] = 0

    i_max = N_buffer/2-1

# Finds the first datapoint index that is larger or equal than now-dt
def find_start(dt):
    global access_lock
    global N_buffer
    global display_window
    global i_max
    global time_array

    start_time = time_array[i_max] - dt
    inds, = np.where(time_array >= start_time)
    return inds[0]



def read_data(logfile):
    global buffer_length
    global channels_array
    global time_array
    global refdate

    timenow_object = datetime.now()
    curr_i = i_max

    if os.path.isfile(logfile):
        for line in reversed(open(logfile).readlines()):
            vals = [b.strip() for b in line.split(";")]
            t = datetime.strptime(vals[0], "%d%m%Y %H:%M:%S")
            chs = vals[1:]

            if (timenow_object-t).total_seconds() > buffer_length:
                break
            
            if curr_i < 0:
                break
            
            if all(chs):
                time_array[curr_i] = (t-refdate).total_seconds()

                for ch in range(len(chs)):
                    channels_array[ch,curr_i] = float(chs[ch])

                curr_i = curr_i - 1

    if curr_i >= 0:
        time_array[0:curr_i+1] = -1e20



if __name__ == '__main__':
    main()
