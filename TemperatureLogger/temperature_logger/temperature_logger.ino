#include <Adafruit_MAX31865.h>

// Use software SPI: CS, DI, DO, CLK
Adafruit_MAX31865 max1 = Adafruit_MAX31865(10, 11, 12, 13);
Adafruit_MAX31865 max2 = Adafruit_MAX31865(9, 11, 12, 13);
Adafruit_MAX31865 max3 = Adafruit_MAX31865(8, 11, 12, 13);

// use hardware SPI, just pass in the CS pin
//Adafruit_MAX31865 max = Adafruit_MAX31865(10);

// The value of the Rref resistor. Use 430.0 for PT100 and 4300.0 for PT1000
#define RREF      430.0
#define RREF3     4300.0
// The 'nominal' 0-degrees-C resistance of the sensor
// 100.0 for PT100, 1000.0 for PT1000
#define RNOMINAL  100.0
#define RNOMINAL3 1000.0

void setup() {
  Serial.begin(115200);
  max1.begin(MAX31865_4WIRE);  // set to 2WIRE or 4WIRE as necessary
  max2.begin(MAX31865_4WIRE);  // set to 2WIRE or 4WIRE as necessary
  max3.begin(MAX31865_4WIRE);  // set to 2WIRE or 4WIRE as necessary

}


void loop() {
  // put your main code here, to run repeatedly:

  char c = Serial.read();
  float T = -500;
  
  if (c >= 0) {
    // Activate the correct sensor
    if (c == 1) {
      T = max1.temperature(RNOMINAL, RREF); 
      check_faults(max1);
    }
    else if (c == 2) {
      T = max2.temperature(RNOMINAL, RREF); 
      check_faults(max2);
    }
    else if (c == 3) {
      T = max3.temperature(RNOMINAL3, RREF3); 
      check_faults(max3);
    }

    else {
      // Error, -500 K will be sent to serial port
    }

    // Send temperature to the serial channel
    Serial.println(T);
  }  
}

uint8_t check_faults(Adafruit_MAX31865 max) {
  // Check any faults
  uint8_t fault = max.readFault();
  
  if (fault) {
    Serial.print("Fault 0x"); Serial.println(fault, HEX);
    if (fault & MAX31865_FAULT_HIGHTHRESH) {
    }
    if (fault & MAX31865_FAULT_LOWTHRESH) {
    }
    if (fault & MAX31865_FAULT_REFINLOW) {
    }
    if (fault & MAX31865_FAULT_REFINHIGH) {
    }
    if (fault & MAX31865_FAULT_RTDINLOW) {
    }
    if (fault & MAX31865_FAULT_OVUV) {
    }
    max.clearFault();   
  }

  return fault;
}
